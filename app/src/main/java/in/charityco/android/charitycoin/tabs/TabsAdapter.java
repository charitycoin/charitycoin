package in.charityco.android.charitycoin.tabs;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.Arrays;
import java.util.List;

import in.charityco.android.charitycoin.R;
import in.charityco.android.charitycoin.tabs.discover.DiscoverCausesFragment;
import in.charityco.android.charitycoin.tabs.feed.FeedFragment;
import in.charityco.android.charitycoin.tabs.profile.UserProfileFragment;


class TabsAdapter extends FragmentPagerAdapter {

    private final Context context;
    private final List<String> tabTitles;
    private final List<String> tabFragments;

    public TabsAdapter(Context context, FragmentManager fm) {
        super(fm);

        this.context = context;

        tabTitles = Arrays.asList(
                context.getString(R.string.tab_home),
                context.getString(R.string.tab_discover),
                context.getString(R.string.tab_profile)
        );

        tabFragments = Arrays.asList(
                FeedFragment.class.getName(),
                DiscoverCausesFragment.class.getName(),
                UserProfileFragment.class.getName()
        );
    }

    @Override
    public Fragment getItem(int position) {
        return Fragment.instantiate(context, tabFragments.get(position));
    }

    @Override
    public int getCount() {
        return tabFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles.get(position);
    }
}
