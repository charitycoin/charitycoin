package in.charityco.android.charitycoin.signin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.facebook.Session;

import in.charityco.android.charitycoin.MainActivity;
import in.charityco.android.charitycoin.api.CharityCoinApi;
import in.charityco.android.charitycoin.api.model.User;
import in.charityco.android.charitycoin.util.Task;


public class SignInActivity extends FragmentActivity implements FacebookSignInFragment.OnSignedInListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            return;
        }

        CharityCoinApi.init(this.getApplicationContext());

        CharityCoinApi.getInstance(this).isLoggedIn(new Task.OnDoneListener<Boolean>() {
            @Override
            public void onDone(Boolean signedIn) {
                if (signedIn) {
                    goToMain();
                } else {
                    showSignInFragment();
                }
            }
        });
    }

    private void showSignInFragment() {
        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, new FacebookSignInFragment())
                .commit();
    }

    @Override
    public void onSignedIn() {
        CharityCoinApi.getInstance(this).login(Session.getActiveSession().getAccessToken(),
                new Task.OnDoneListener<User>() {
                    @Override
                    public void onDone(User user) {
                        goToMain();
                    }
                });
    }

    private void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
