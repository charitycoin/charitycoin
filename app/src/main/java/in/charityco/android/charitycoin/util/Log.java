package in.charityco.android.charitycoin.util;


import android.support.annotation.NonNull;

import java.util.Hashtable;
import java.util.Map;

public class Log {

    private static final int MIN_LOG_LEVEL = android.util.Log.INFO;

    private static final String BASE_TAG = "CharityCoin";

    private static final Map<String, Log> loggers = new Hashtable<>();

    private final String tag;

    private Log(String tag) {
        this.tag = tag;
    }

    public void info(Object... args) {
        log(android.util.Log.INFO, tag, args);
    }

    public void debug(Object... args) {
        log(android.util.Log.DEBUG, tag, args);
    }

    public void error(Object... args) {
        log(android.util.Log.ERROR, tag, args);
    }

    public void error(Throwable e) {
        error(tag, e.getMessage(), android.util.Log.getStackTraceString(e));
    }

    public static Log getLog(String tag) {
        synchronized (loggers) {
            if (!loggers.containsKey(tag)) {
                loggers.put(tag, new Log(BASE_TAG + "/" + tag));
            }

            return loggers.get(tag);
        }
    }

    public static void log(int priority, String tag, Object... msgs) {
        if (priority < MIN_LOG_LEVEL) {
            return;
        }

        StringBuilder builder = new StringBuilder();
        for (Object msg : msgs) {
            builder.append(msg).append(' ');
        }

        android.util.Log.println(priority, tag, builder.toString());
    }

}
