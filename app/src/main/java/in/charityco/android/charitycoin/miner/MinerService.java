package in.charityco.android.charitycoin.miner;

import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.PowerManager;

import in.charityco.android.coinminer.CPUMiner;
import in.charityco.android.coinminer.CPUMinerRunnable;


/*public class MinerService extends IntentService {

    public static final String ACTION_START_MINER = "in.charityco.android.charitycoin.action.START";
    public static final String ACTION_STOP_MINER = "in.charityco.android.charitycoin.action.STOP";

    public MinerService() {
        super("MinerService");
    }

    public static void startCpuMiner(Context context) {
        Intent intent = new Intent(context, MinerService.class);
        intent.setAction(ACTION_START_MINER);
        context.startService(intent);
    }

    public static void stopCpuMiner(Context context) {
        Intent intent = new Intent(context, MinerService.class);
        intent.setAction(ACTION_STOP_MINER);
        context.startService(intent);
    }

    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_START_MINER.equals(action)) {
                startMiner();
            } else if (ACTION_STOP_MINER.equals(action)) {
                stopMiner();
            }
        }
    }

    private void startMiner() {
        Intent intent = new Intent(ACTION_START_MINER);
        sendBroadcast(intent);

        MinerNotification.showStickyNotification(getBaseContext());
//        CPUMiner.startMiner();
        new Thread(new CPUMinerRunnable()).start();
    }

    private void stopMiner() {
        Intent intent = new Intent(ACTION_STOP_MINER);
        sendBroadcast(intent);

        MinerNotification.dismissStickyNotification(getBaseContext());
        CPUMiner.stopMiner();
    }

}*/

public class MinerService extends Service {

    public static final String ACTION_START_MINER = "in.charityco.android.charitycoin.action.START";
    public static final String ACTION_STOP_MINER = "in.charityco.android.charitycoin.action.STOP";
    private PowerManager.WakeLock wakeLock;

    public static void startCpuMiner(Context context) {
        Intent intent = new Intent(context, MinerService.class);
        intent.setAction(ACTION_START_MINER);
        context.startService(intent);
    }

    public static void stopCpuMiner(Context context) {
        Intent intent = new Intent(context, MinerService.class);
        intent.setAction(ACTION_STOP_MINER);
        context.startService(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(
                PowerManager.PARTIAL_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE,
                MinerService.class.getSimpleName());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handleIntent(intent);
        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (CPUMiner.isRunning()) {
            CPUMiner.stopMiner();
        }
    }

    private void startMiner() {
        wakeLock.acquire();

        Intent intent = new Intent(ACTION_START_MINER);
        sendBroadcast(intent);

        startForeground(123, MinerNotification.showStickyNotification(this));

        new MinerAsyncTask().execute();
    }

    private void stopMiner() {
        Intent intent = new Intent(ACTION_STOP_MINER);
        sendBroadcast(intent);

        stopForeground(true);

        CPUMiner.stopMiner();

        if (wakeLock != null && wakeLock.isHeld()) {
            wakeLock.release();
        }
    }

    private void handleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_START_MINER.equals(action)) {
                startMiner();
            } else if (ACTION_STOP_MINER.equals(action)) {
                stopMiner();
            }
        }
    }
}
