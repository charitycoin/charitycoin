package in.charityco.android.charitycoin.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import in.charityco.android.charitycoin.R;
import in.charityco.android.charitycoin.util.Task;
import in.charityco.android.charitycoin.util.Log;

public class LoadBitmap extends Task<String, Void, Bitmap> {

    public LoadBitmap(OnDoneListener<Bitmap> listener) {
        super(listener);
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        HttpURLConnection connection = null;
        Bitmap bmp = null;

        try {
            connection = (HttpURLConnection) new URL(params[0]).openConnection();
            bmp = BitmapFactory.decodeStream(connection.getInputStream());
        } catch (IOException e) {
            log.error(e);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        return bmp;
    }

    public static void loadIntoImageView(final ImageView imageView, final String url) {
        imageView.setTag(R.id.load_bitmap_task_image_view_tag, url);

        new LoadBitmap(new OnDoneListener<Bitmap>() {
            @Override
            public void onDone(Bitmap bitmap) {
                if (url.equals(imageView.getTag(R.id.load_bitmap_task_image_view_tag))) {
                    imageView.setImageBitmap(bitmap);
                }
            }
        }).execute(url);
    }
}
