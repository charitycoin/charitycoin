package in.charityco.android.charitycoin.miner;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.os.Build;

import in.charityco.android.charitycoin.MainActivity;
import in.charityco.android.charitycoin.R;
import in.charityco.android.charitycoin.api.CharityCoinApi;
import in.charityco.android.charitycoin.api.model.Cause;

class MinerNotification {

    private static final int NOTIFICATION_ID = 0;

    public static Notification showStickyNotification(Context context) {
        Intent resultIntent = new Intent(context, MainActivity.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Resources resources = context.getResources();

        String title = resources.getString(R.string.notification_title);
        String body = null;

        Cause supportedCause = CharityCoinApi.getInstance(context).getSupportedCause();
        if (supportedCause != null) {
            body = resources.getString(R.string.notification_body,
                    CharityCoinApi.getInstance(context).getSupportedCause().title);
        }

        Notification.Builder builder = new Notification.Builder(context);

        Intent stopMinerIntent = new Intent(context, MinerService.class);
        stopMinerIntent.setAction(MinerService.ACTION_STOP_MINER);

        builder.setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(R.drawable.logo_notification)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.launcher))
                .setOngoing(true)
                .setContentIntent(resultPendingIntent)
                .addAction(
                        android.R.drawable.ic_lock_power_off,
                        resources.getString(R.string.notification_stop_miner),
                        PendingIntent.getService(context, 0, stopMinerIntent, 0));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setCategory(Notification.CATEGORY_PROGRESS);
        }

//        notificationManager.notify(NOTIFICATION_ID, builder.build());

        return builder.build();
    }

    public static void dismissStickyNotification(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
    }

}
