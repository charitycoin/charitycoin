package in.charityco.android.charitycoin.api;

import in.charityco.android.charitycoin.api.http.Http;
import in.charityco.android.charitycoin.api.model.parser.JsonToModelParser;
import in.charityco.android.charitycoin.api.model.parser.ParseException;
import in.charityco.android.charitycoin.util.Log;
import in.charityco.android.charitycoin.util.Task;


class ParseModelOnDone<Model> implements Task.OnDoneListener<Http.Result> {

    private static final Log log = Log.getLog(ParseModelOnDone.class.getSimpleName());

    private final JsonToModelParser<Model> parser;
    private final Task.OnDoneListener<Model> listener;

    ParseModelOnDone(JsonToModelParser<Model> parser, Task.OnDoneListener<Model> listener) {
        this.parser = parser;
        this.listener = listener;
    }

    @Override
    public void onDone(Http.Result result) {
        Model model = null;

        if (result == null) {
            log.info("No result returned");
            return;
        }

        if (result.successful) {
            log.debug("Parsing", result.output);

            try {
                model = parser.parseJson(result.output);
            } catch (ParseException e) {
                log.error(e);
            }
        } else {
            log.error("Got an error", result.error);
        }

        if (listener != null) {
            listener.onDone(model);
        }
    }
}
