package in.charityco.android.charitycoin.api.model.parser;


public class ParseException extends Exception {

    public ParseException(Throwable tr) {
        super(tr);
    }
}
