package in.charityco.android.charitycoin.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import in.charityco.android.charitycoin.api.model.parser.JsonToModelParser;
import in.charityco.android.charitycoin.api.model.parser.ModelToJsonParser;
import in.charityco.android.charitycoin.api.model.parser.ParseException;
import in.charityco.android.charitycoin.util.Log;


public class Cause {

    private static final DateFormat ISO_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    public final String id;
    public final String pictureUrl;
    public final String title;
    public final String shortDescription;
    public final Date startDate;
    public final Date endDate;
    public final int supporters;
    public final double goal;
    public final String goalUnit;
    public final double fundsRaised;
    public final String category;
    public final int friends;
    public final String charityName;
    public final String charityId;
    public final int myDonation;

    public static final JsonToModelParser<Cause> fromJsonParser = new JsonToModelParser<Cause>() {
        @Override
        public Cause parseJson(String json) throws ParseException {
            try {
                return fromJsonObject(new JSONObject(json));
            } catch (JSONException | java.text.ParseException e) {
                throw new ParseException(e);
            }
        }
    };

    public static final JsonToModelParser<List<Cause>> fromJsonListParser = new JsonToModelParser<List<Cause>>() {
        private final Log log = Log.getLog(getClass().getSimpleName());

        @Override
        public List<Cause> parseJson(String json) throws ParseException {
            List<Cause> causes = new ArrayList<>();

            try {
                JSONArray array = new JSONArray(json);

                for (int i = 0; i < array.length(); ++i) {
                    try {
                        causes.add(fromJsonObject(array.getJSONObject(i)));
                    } catch (JSONException | java.text.ParseException e) {
                        log.error(e);
                    }
                }
            } catch (JSONException e) {
                throw new ParseException(e);
            }

            return causes;
        }
    };

    public static final ModelToJsonParser<Cause> toJsonParser = new ModelToJsonParser<Cause>() {
        @Override
        public String parseModel(Cause cause) {
            JSONStringer stringer = new JSONStringer();

            try {
                return stringer.object()
                        .key("id").value(cause.id)
                        .key("pictureUrl").value(cause.pictureUrl)
                        .key("title").value(cause.title)
                        .key("shortDescription").value(cause.shortDescription)
                        .key("startDate").value(ISO_FORMAT.format(cause.startDate))
                        .key("endDate").value(ISO_FORMAT.format(cause.endDate))
                        .key("supporters").value(cause.supporters)
                        .key("goal")
                            .object()
                            .key("amount").value(cause.goal)
                            .key("amountRaised").value(cause.fundsRaised)
                            .key("unit").value(cause.goalUnit)
                            .endObject()
                        .key("fundsRaised").value(cause.fundsRaised)
                        .key("category").value(cause.category)
                        .key("friends").value(cause.friends)
                        .key("charityName").value(cause.charityName)
                        .key("charityId").value(cause.charityId)
                        .endObject()
                        .toString();
            } catch (JSONException e) {
            }

            return "";
        }
    };


    public Cause(String id, String pictureUrl, String title, String shortDescription, Date startDate, Date endDate, int supporters, double goal, String goalUnit, double fundsRaised, String category, int friends, String charityName, String charityId, int myDonation) {
        this.id = id;
        this.pictureUrl = pictureUrl;
        this.title = title;
        this.shortDescription = shortDescription;
        this.startDate = startDate;
        this.endDate = endDate;
        this.supporters = supporters;
        this.goal = goal;
        this.goalUnit = goalUnit;
        this.fundsRaised = fundsRaised;
        this.category = category;
        this.friends = friends;
        this.charityName = charityName;
        this.charityId = charityId;
        this.myDonation = myDonation;
    }

    public int getDaysLeft() {
        Date now = Calendar.getInstance().getTime();
        long delta = endDate.getTime() - now.getTime();

        return 1 + (int) (delta / (24 * 60 * 60 * 1000));
    }

    public int getGoalPercentDone() {
        return (int) (fundsRaised / goal) * 100;
    }

    @Override
    public String toString() {
        return toJsonParser.parseModel(this);
    }

    public static Cause fromJsonObject(JSONObject object) throws JSONException, java.text.ParseException {
        String id = object.getString("id");
        String charityId = object.getString("charityId");
        String title = object.getString("title");
        String shortDescription = object.getString("shortDescription");
        String pictureUrl = object.has("pictureURL") ? object.getString("pictureURL") : null;

        Date startDate = ISO_FORMAT.parse(object.getString("startDate"));
        Date endDate = ISO_FORMAT.parse(object.getString("endDate"));

        JSONObject goal = object.getJSONObject("goal");
        double goalAmount = goal.getDouble("amount");
        String goalUnit = "Ł"; //goal.has("unit") ? goal.getString("unit") : "$";
        double funds = goal.has("amountRaised") ? goal.getDouble("amountRaised") : 0;

        int supporters = 10;
        String category = object.getString("category");

        int friends = object.has("friends") ? object.getInt("friends") : 0;

        String charityName = object.getString("charityName");

        int myDonation = object.has("donation") ? object.getInt("donation") : 0;

        return new Cause(id, pictureUrl, title, shortDescription, startDate, endDate, supporters, goalAmount, goalUnit, funds, category, friends, charityName, charityId, myDonation);
    }
}
