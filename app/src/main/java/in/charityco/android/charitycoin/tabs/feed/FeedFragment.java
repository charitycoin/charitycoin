package in.charityco.android.charitycoin.tabs.feed;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import in.charityco.android.charitycoin.R;
import in.charityco.android.charitycoin.ShowCauseDetails;
import in.charityco.android.charitycoin.api.CharityCoinApi;
import in.charityco.android.charitycoin.api.model.Cause;
import in.charityco.android.charitycoin.api.model.FeedItem;
import in.charityco.android.charitycoin.miner.MinerService;
import in.charityco.android.charitycoin.util.Log;
import in.charityco.android.charitycoin.util.Task;
import in.charityco.android.coinminer.CPUMiner;


public class FeedFragment extends ListFragment {

    private static final Log log = Log.getLog(FeedFragment.class.getSimpleName());

    private Task.OnDoneListener<List<FeedItem>> onFeedLoaded = new Task.OnDoneListener<List<FeedItem>>() {
        @Override
        public void onDone(List<FeedItem> feedItems) {
            if (feedItems == null) {
                log.error("Got a null feedItems");
                return;
            }

            setListAdapter(new FeedAdapter(getActivity(), feedItems));
        }
    };

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case MinerService.ACTION_START_MINER:
                    hideMinerWarning();
                    break;

                case MinerService.ACTION_STOP_MINER:
                    showMinerWarning();
                    break;

                default:
                    log.info("Unknown intent action", action);
                    break;
            }
        }
    };

    private ShowCauseDetails causeDetails;

    private View minerWarning;

    public FeedFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            CharityCoinApi.getInstance(getActivity()).getFeed(onFeedLoaded);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ListView listView = getListView();

        minerWarning = LayoutInflater.from(getActivity()).inflate(R.layout.warning, listView, false);
        minerWarning.setVisibility(View.VISIBLE);

        ((TextView) minerWarning.findViewById(R.id.warning_text)).setText(
                R.string.warning_miner_not_working
        );
    }

    @Override
    public FeedAdapter getListAdapter() {
        return (FeedAdapter) super.getListAdapter();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Cause cause = getListAdapter().getItem(position - getListView().getHeaderViewsCount()).cause;

        if (causeDetails != null) {
            causeDetails.showCauseDetails(cause.id);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            causeDetails = (ShowCauseDetails) activity;
        } catch (ClassCastException e) {
            log.error(e);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        causeDetails = null;
    }

    @Override
    public void onStart() {
        super.onStart();

        IntentFilter filter = new IntentFilter();
        filter.addAction(MinerService.ACTION_START_MINER);
        filter.addAction(MinerService.ACTION_STOP_MINER);

        getActivity().registerReceiver(receiver, filter);
    }

    @Override
    public void onStop() {
        super.onStop();

        getActivity().unregisterReceiver(receiver);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (CPUMiner.isRunning()) {
            hideMinerWarning();
        } else {
            showMinerWarning();
        }
    }

    private void showMinerWarning() {
        if (minerWarning.getParent() == null) {
            getListView().addHeaderView(minerWarning, null, false);
        }
    }

    private void hideMinerWarning() {
        if (minerWarning.getParent() != null) {
            getListView().removeHeaderView(minerWarning);
        }
    }
}
