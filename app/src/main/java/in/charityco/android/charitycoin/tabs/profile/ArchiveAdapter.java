package in.charityco.android.charitycoin.tabs.profile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import in.charityco.android.charitycoin.R;
import in.charityco.android.charitycoin.api.model.Cause;
import in.charityco.android.charitycoin.util.Layout;


public class ArchiveAdapter extends BaseAdapter {

    private final Context context;
    private final List<Cause> archive;

    public ArchiveAdapter(Context context, List<Cause> archive) {
        this.context = context;
        this.archive = archive;
    }

    @Override
    public int getCount() {
        return archive.size();
    }

    @Override
    public Cause getItem(int position) {
        return archive.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.cause_archive_item, parent, false);
        }

        Layout.fillArchiveItem(context, getItem(position), view);

        return view;
    }
}
