package in.charityco.android.charitycoin.api.model.parser;


public interface JsonToModelParser<Model> {
    public Model parseJson(String json) throws ParseException;
}
