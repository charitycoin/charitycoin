package in.charityco.android.charitycoin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import in.charityco.android.charitycoin.signin.SignInActivity;


public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
    }
}
