package in.charityco.android.charitycoin.util;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.makeramen.RoundedImageView;

import in.charityco.android.charitycoin.R;
import in.charityco.android.charitycoin.api.model.Cause;


public class Layout {

    public static void fillCauseCard(Cause cause, View view) {
        Context context = view.getContext();

        TextView causeTitle = (TextView) view.findViewById(R.id.cause_title);
        TextView causeCharity = (TextView) view.findViewById(R.id.cause_charity);
        RoundedImageView causeImage = (RoundedImageView) view.findViewById(R.id.cause_picture);

        causeTitle.setText(cause.title);
        causeCharity.setText(context.getString(R.string.cause_started_by_charity, cause.charityName));

        if (cause.pictureUrl != null) {
            LoadBitmap.loadIntoImageView(causeImage, cause.pictureUrl);
        }
    }

    public static void fillArchiveItem(Context context, Cause cause, View view) {
        ((TextView) view.findViewById(R.id.user_funding_raised)).setText(
                context.getResources().getString(
                        R.string.user_cause_funds_raised, cause.goalUnit + cause.myDonation)
        );

        Layout.fillCauseCard(cause, view);
    }
}
