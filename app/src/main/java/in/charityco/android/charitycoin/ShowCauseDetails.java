package in.charityco.android.charitycoin;


public interface ShowCauseDetails {

    public void showCauseDetails(String causeId);

}
