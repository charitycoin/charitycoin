package in.charityco.android.charitycoin.api.model;


import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import in.charityco.android.charitycoin.api.model.parser.JsonToModelParser;
import in.charityco.android.charitycoin.api.model.parser.ModelToJsonParser;
import in.charityco.android.charitycoin.api.model.parser.ParseException;

public class User {

    public final String id;
    public final String userId;
    public final String username;
    public final String securityLevel;
    public final String pictureUrl;
    public final String name;

    public static final JsonToModelParser<User> fromJsonParser = new JsonToModelParser<User>() {

        @Override
        public User parseJson(String json) throws ParseException {
            try {
                return fromJsonObject(new JSONObject(json));
            } catch (JSONException e) {
                throw new ParseException(e);
            }
        }
    };

    public static final ModelToJsonParser<User> toJsonParser = new ModelToJsonParser<User>() {
        @Override
        public String parseModel(User m) {
            try {
                return new JSONStringer()
                        .object()
                        .key("id").value(m.id)
                        .key("userId").value(m.userId)
                        .key("username").value(m.username)
                        .key("pictureURL").value(m.pictureUrl)
                        .key("name").value(m.name)
                        .key("securityLevel").value(m.securityLevel)
                        .endObject()
                        .toString();
            } catch (JSONException e) {
                // Shouldn't happen
                return "";
            }
        }
    };


    public User(String id, String userId, String username, String name, String securityLevel, String pictureUrl) {
        this.id = id;
        this.userId = userId;
        this.username = username;
        this.name = name;
        this.securityLevel = securityLevel;
        this.pictureUrl = pictureUrl;
    }

    public static User fromJsonObject(JSONObject object) throws JSONException, ParseException {
        String securityLevel = object.getString("securityLevel");
        String id = object.getString("id");
        String userId = object.getString("userId");
        String username = object.getString("username");

        // TODO hardcoded values
        String pictureUrl = object.has("pictureURL") ? object.getString("pictureURL") :
                "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfp1/t31.0-1/c362.106.1324.1324/s480x480/664751_416711591715309_1796614799_o.jpg";
        String name = object.has("name") ? object.getString("name") : "Alex Gherghisan";

        return new User(id, userId, username, name, securityLevel, pictureUrl);
    }

    @Override
    public String toString() {
        return toJsonParser.parseModel(this);
    }
}
