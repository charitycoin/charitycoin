package in.charityco.android.charitycoin.api.model;


import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

public class Charity {

    public static enum Type {
        PEOPLE,
        ANIMALS,
        NATURE
    }

    private static final String TAG = "Charity: " ;

    private String id;
    private String about;
    private String description;
    private String coverPhotoUrlString;
    private String mission;
    private String name;
    private String city;
    private String country;
    private String username;
    private String website;

    public Charity(JSONObject jsonObject) {
        try {

            this.id = jsonObject.has("id") ? jsonObject.getString("id") : "";
            this.about = jsonObject.has("about") ? jsonObject.getString("about") : "";
            this.description = jsonObject.has("description") ? jsonObject.getString("description") : "";
            this.mission = jsonObject.has("mission") ? jsonObject.getString("mission") : "";
            this.name = jsonObject.has("name") ? jsonObject.getString("name") : "";
            this.username = jsonObject.has("username") ? jsonObject.getString("username") : "";
            this.website = jsonObject.has("website") ? jsonObject.getString("website") : "";

            JSONObject coverObject = jsonObject.has("cover") ? jsonObject.getJSONObject("cover") : null;
            if(coverObject != null) {
                this.coverPhotoUrlString = coverObject.has("source") ? coverObject.getString("source") : "";
            }

            JSONObject locationObject = jsonObject.has("location") ? jsonObject.getJSONObject("location") : null;
            if(locationObject != null) {
                this.city = locationObject.has("city") ? locationObject.getString("city") : "";
                this.country = locationObject.has("country") ? locationObject.getString("country") : "";
            }

        }catch (JSONException e) {
            Log.e(TAG, "Exception while getting charity details: " + e.getMessage());
        }
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCoverPhotoUrlString() {
        return coverPhotoUrlString;
    }

    public void setCoverPhotoUrlString(String coverPhotoUrlString) {
        this.coverPhotoUrlString = coverPhotoUrlString;
    }

    public String getMission() {
        return mission;
    }

    public void setMission(String mission) {
        this.mission = mission;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}
