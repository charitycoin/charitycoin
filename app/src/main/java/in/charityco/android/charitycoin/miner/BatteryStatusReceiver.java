package in.charityco.android.charitycoin.miner;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BatteryStatusReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_POWER_CONNECTED)) {
            MinerService.startCpuMiner(context.getApplicationContext());
        } else {
            MinerService.stopCpuMiner(context.getApplicationContext());
        }
    }
}
