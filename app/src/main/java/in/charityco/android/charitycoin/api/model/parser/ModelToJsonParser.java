package in.charityco.android.charitycoin.api.model.parser;

public interface ModelToJsonParser<Model> {

    public String parseModel(Model m);

}
