package in.charityco.android.charitycoin.tabs.discover;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.makeramen.RoundedImageView;

import java.util.Arrays;
import java.util.List;

import in.charityco.android.charitycoin.R;
import in.charityco.android.charitycoin.ShowCauseDetails;
import in.charityco.android.charitycoin.api.CharityCoinApi;
import in.charityco.android.charitycoin.api.model.Cause;
import in.charityco.android.charitycoin.api.model.UserProfile;
import in.charityco.android.charitycoin.util.LoadBitmap;
import in.charityco.android.charitycoin.util.Log;
import in.charityco.android.charitycoin.util.Task;

public class DiscoverCausesFragment extends ListFragment {

    private static final Log log = Log.getLog(DiscoverCausesFragment.class.getSimpleName());

    private List<String> categoryValues = Arrays.asList("Any", "People", "Animals", "Nature");

    private Button categorySelectorBtn;

    private AlertDialog.Builder builder;

    private ShowCauseDetails causeDetails;

    private View.OnClickListener onClickCategorySelectorBtn = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            builder.show();
        }
    };

    private DialogInterface.OnClickListener onSelectCauseCategory = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            setSelectedCauseCategory(categoryValues.get(which));
        }
    };

    private Task.OnDoneListener<List<Cause>> onCausesLoaded = new Task.OnDoneListener<List<Cause>>() {
        @Override
        public void onDone(List<Cause> causes) {
            if (causes == null) {
                log.error("Got a null causes");
                return;
            }

            setListAdapter(new CausesAdapter(getActivity(), causes));
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            CharityCoinApi.getInstance(getActivity()).getCausesByCategory(null, onCausesLoaded);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ListView listView = getListView();

        View header = getLayoutInflater(savedInstanceState)
                .inflate(R.layout.fragment_discover_causes_list_header, listView, false);

        listView.addHeaderView(header, null, false);

        ((TextView) header.findViewById(R.id.user_message)).setText(R.string.title_help_a_cause);

        categorySelectorBtn = (Button) header.findViewById(R.id.discover_causes_category_select_btn);
        categorySelectorBtn.setOnClickListener(onClickCategorySelectorBtn);

        UserProfile profile = CharityCoinApi.getInstance(getActivity()).getUserProfile();
        if (profile.avatarURL != null) {
            RoundedImageView userPicture = (RoundedImageView) header.findViewById(R.id.user_picture);
            LoadBitmap.loadIntoImageView(userPicture, profile.avatarURL);
        }

        if (savedInstanceState == null) {
            setSelectedCauseCategory(categoryValues.get(0));
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            causeDetails = (ShowCauseDetails) activity;
        } catch (ClassCastException e) {
            log.error(e);
        }

        initCauseCategoryDialogBuilder(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        builder = null;
        causeDetails = null;
    }

    @Override
    public CausesAdapter getListAdapter() {
        return (CausesAdapter) super.getListAdapter();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // -1 because of the list header
        Cause cause = getListAdapter().getItem(position - 1);

        if (causeDetails != null) {
            causeDetails.showCauseDetails(cause.id);
        }
    }

    private void setSelectedCauseCategory(String causeCategory) {
        categorySelectorBtn.setText(getString(R.string.discover_causes_category_btn, causeCategory));

        CausesAdapter adapter = getListAdapter();
        if (adapter != null) {
            adapter.getFilter().filter(causeCategory);
        }
    }

    private void initCauseCategoryDialogBuilder(Context context) {
        builder = new AlertDialog.Builder(context);

        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<>(
                context,
                android.R.layout.simple_list_item_1,
                categoryValues);

        builder.setAdapter(categoryAdapter, onSelectCauseCategory);
    }
}
