package in.charityco.android.charitycoin.api.model;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.charityco.android.charitycoin.api.model.parser.JsonToModelParser;
import in.charityco.android.charitycoin.api.model.parser.ParseException;
import in.charityco.android.charitycoin.util.Log;

public class FeedItem {

    public static enum Type {
        NEW_CAUSE,
        END_CAUSE,
        SHARED_CAUSE,
        MLS_PROGRESS,
        MLS_FRIEND_SUPPORT,
        MLS_USER_SUPPORT
    }

    public static enum Privacy {
        PRIVATE,
        PUBLIC,
    }

    private static final Log log = Log.getLog(FeedItem.class.getSimpleName());

    public final String id;
    public final Privacy privacy;
    public final Type type;
    public final Cause cause;
    public final String userId;
    public final String message;

    public FeedItem(String id, Privacy privacy, Type type, Cause cause, String userId, String message) {
        this.id = id;
        this.privacy = privacy;
        this.type = type;
        this.cause = cause;
        this.userId = userId;
        this.message = message;
    }

    public static final JsonToModelParser<FeedItem> fromJsonParser = new JsonToModelParser<FeedItem>() {
        @Override
        public FeedItem parseJson(String json) throws ParseException {
            try {
                return fromJsonObject(new JSONObject(json));
            } catch (JSONException | java.text.ParseException e) {
                throw new ParseException(e);
            }
        }
    };

    public static final JsonToModelParser<List<FeedItem>> fromJsonListParser = new JsonToModelParser<List<FeedItem>>() {
        @Override
        public List<FeedItem> parseJson(String json) throws ParseException {
            List<FeedItem> list = new ArrayList<>();

            try {
                JSONArray array = new JSONArray(json);

                for (int i = 0; i < array.length(); ++i) {
                    try {
                        list.add(fromJsonObject(array.getJSONObject(i)));
                    } catch (java.text.ParseException | JSONException e) {
                        log.error(e);
                    }
                }

            } catch (JSONException e) {
                throw new ParseException(e);
            }

            return list;
        }
    };

    public static FeedItem fromJsonObject(JSONObject object) throws JSONException, java.text.ParseException {
        String id = object.getString("id");
        Privacy privacy = Privacy.valueOf(object.getString("privacy"));
        Type type = Type.valueOf(object.getString("type"));
        Cause cause = Cause.fromJsonObject(object.getJSONObject("cause"));
        String userId = object.has("userId") ? object.getString("userId") : null;
        String message = object.getString("message");

        return new FeedItem(id, privacy, type, cause, userId, message);
    }
}
