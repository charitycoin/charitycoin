package in.charityco.android.charitycoin.tabs.profile;


import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.List;

import in.charityco.android.charitycoin.R;
import in.charityco.android.charitycoin.api.CharityCoinApi;
import in.charityco.android.charitycoin.api.model.Cause;
import in.charityco.android.charitycoin.api.model.User;
import in.charityco.android.charitycoin.api.model.UserProfile;
import in.charityco.android.charitycoin.util.Layout;
import in.charityco.android.charitycoin.util.LoadBitmap;
import in.charityco.android.charitycoin.util.Task;


public class UserProfileFragment extends ListFragment {

    private static final int MAX_CPU = 25;

    private User user;
    private UserProfile profile;
    private Cause supportedCause;

    private View header;

    private TextView cpuUsageTitle;
    SeekBar cpuUsageSlider;

    private Task.OnDoneListener<List<Cause>> onArchiveLoaded = new Task.OnDoneListener<List<Cause>>() {
        @Override
        public void onDone(List<Cause> causes) {
            if (causes != null) {
                setListAdapter(new ArchiveAdapter(getActivity(), causes));
            } else {
                setListAdapter(null);
            }

            checkEmptyList();
        }
    };

    private SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            CharityCoinApi.getInstance(getActivity()).setMinerCPU(progress);
            showSeekBarProgress(progress);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        user = CharityCoinApi.getInstance(getActivity()).getUser();
        profile = CharityCoinApi.getInstance(getActivity()).getUserProfile();

        CharityCoinApi.getInstance(getActivity()).getArchive(onArchiveLoaded);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ListView listView = getListView();

        header = LayoutInflater.from(getActivity()).inflate(
                R.layout.fragment_user_profile_header, listView, false);

        listView.addHeaderView(header, null, false);

        updateUserUi();
        updateCauseUi();
        setupSeekBar();

        int storedPercent = CharityCoinApi.getInstance(getActivity()).getMinerCPU();
        cpuUsageSlider.setProgress(storedPercent);
    }

    @Override
    public void onResume() {
        super.onResume();

        supportedCause = CharityCoinApi.getInstance(getActivity()).getSupportedCause();

        if (supportedCause == null) {
            updateCauseUi();
        } else {
            CharityCoinApi.getInstance(getActivity()).getCause(supportedCause.id, new Task.OnDoneListener<Cause>() {
                @Override
                public void onDone(Cause cause) {
                    supportedCause = cause;
                    updateCauseUi();
                }
            });
        }

        checkEmptyList();
    }

    private void checkEmptyList() {
        ListAdapter listAdapter = getListAdapter();
        if (listAdapter != null && listAdapter.getCount() == 0) {
            header.findViewById(R.id.user_no_archive).setVisibility(View.VISIBLE);
        }
    }

    private void updateUserUi() {
        ((TextView) header.findViewById(R.id.user_message)).setText(
                profile.firstName + " " + profile.lastName);

        if (profile.avatarURL != null) {
            LoadBitmap.loadIntoImageView(
                    (ImageView) header.findViewById(R.id.user_picture), profile.avatarURL);
        }
    }

    private void updateCauseUi() {
        if (header == null) {
            return;
        }

        if (supportedCause == null) {
            showNoCauseUI();
        } else {
            showCauseUI();
        }
    }

    private void showNoCauseUI() {
        View warning = header.findViewById(R.id.user_no_cause_supported);
        View cause = header.findViewById(R.id.user_cause_supported);

        cause.setVisibility(View.GONE);
        warning.setVisibility(View.VISIBLE);

        ((TextView) warning.findViewById(R.id.warning_text)).setText(
                R.string.warning_no_cause_supported);
    }

    private void showCauseUI() {
        View warning = header.findViewById(R.id.user_no_cause_supported);
        View cause = header.findViewById(R.id.user_cause_supported);

        cause.setVisibility(View.VISIBLE);
        warning.setVisibility(View.GONE);

        Layout.fillArchiveItem(getActivity(), supportedCause, cause);
    }

    private void showSeekBarProgress(int progress) {
        String percent = progress + "%";
        String text = getActivity().getString(R.string.user_miner_cpu_usage, percent);

        int spanStart = text.indexOf(percent);

        cpuUsageTitle.setText(text, TextView.BufferType.SPANNABLE);
        Spannable span = (Spannable) cpuUsageTitle.getText();
        span.setSpan(
                new ForegroundColorSpan(getActivity().getResources().getColor(R.color.accent)),
                spanStart,
                spanStart + percent.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    private void setupSeekBar() {
        View cause = header.findViewById(R.id.user_cause_supported);

        cpuUsageTitle = (TextView) cause.findViewById(R.id.user_miner_cpu_usage);
        cpuUsageSlider = (SeekBar) cause.findViewById(R.id.user_miner_cpu_usage_slider);
        cpuUsageSlider.setMax(MAX_CPU);
        cpuUsageSlider.setOnSeekBarChangeListener(onSeekBarChangeListener);
    }
}
