package in.charityco.android.charitycoin.tabs.feed;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import in.charityco.android.charitycoin.R;
import in.charityco.android.charitycoin.api.model.FeedItem;
import in.charityco.android.charitycoin.util.Layout;


class FeedAdapter extends BaseAdapter {

    private final Context context;
    private final List<FeedItem> items;

    public FeedAdapter(Context context, List<FeedItem> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public FeedItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.feed_item, parent, false);
        }

        FeedItem item = getItem(position);

        ((TextView) view.findViewById(R.id.feed_item_message)).setText(item.message);

        Layout.fillCauseCard(item.cause, view);

        return view;
    }
}
