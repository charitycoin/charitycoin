package in.charityco.android.charitycoin.miner;

import android.os.AsyncTask;

import in.charityco.android.coinminer.CPUMiner;


public class MinerAsyncTask extends AsyncTask<Void, Void, Void> {

    @Override
    protected Void doInBackground(Void... params) {
        CPUMiner.startMiner();
        return null;
    }
}
