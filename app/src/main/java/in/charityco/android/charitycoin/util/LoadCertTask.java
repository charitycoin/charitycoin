package in.charityco.android.charitycoin.util;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import in.charityco.android.charitycoin.R;
import in.charityco.android.charitycoin.util.Task;


public class LoadCertTask extends Task<Void, Void, SSLContext> {

    final Context context;

    public LoadCertTask(Context context, OnDoneListener<SSLContext> listener) {
        super(listener);
        this.context = context;
    }

    @Override
    protected SSLContext doInBackground(Void... voids) {
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream stream = context.getResources().openRawResource(R.raw.charityco);
            Certificate ca = cf.generateCertificate(stream);

            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            // Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            // Create an SSLContext that uses our TrustManager
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, tmf.getTrustManagers(), null);

            return sslContext;
        } catch (CertificateException | KeyManagementException | NoSuchAlgorithmException | KeyStoreException | IOException e) {
            log.error(e);
        }

        return null;
    }
}
