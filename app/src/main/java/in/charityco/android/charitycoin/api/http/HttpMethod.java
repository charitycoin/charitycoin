package in.charityco.android.charitycoin.api.http;


public enum HttpMethod {
    GET,
    POST,
    PUT,
    DELETE
}
