package in.charityco.android.charitycoin.util;

import android.os.AsyncTask;


public abstract class Task<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    public static interface OnDoneListener<Result> {
        public void onDone(Result result);
    }

    protected final Log log = Log.getLog(getClass().getSimpleName());

    protected final OnDoneListener<Result> listener;

    protected Task(OnDoneListener<Result> listener) {
        this.listener = listener;
    }

    @Override
    protected void onPostExecute(Result result) {
        if (listener != null) {
            listener.onDone(result);
        }
    }
}
