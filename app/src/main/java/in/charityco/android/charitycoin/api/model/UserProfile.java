package in.charityco.android.charitycoin.api.model;


import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import in.charityco.android.charitycoin.api.model.parser.JsonToModelParser;
import in.charityco.android.charitycoin.api.model.parser.ModelToJsonParser;
import in.charityco.android.charitycoin.api.model.parser.ParseException;

public class UserProfile {

    public final String country;
    public final String city;
    public final String id;
    public final String email;
    public final String username;
    public final String firstName;
    public final String lastName;
    public final String avatarURL;

    public UserProfile(String country, String city, String id, String email, String username, String firstName, String lastName, String avatarURL) {
        this.country = country;
        this.city = city;
        this.id = id;
        this.email = email;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.avatarURL = avatarURL;
    }

    public final static JsonToModelParser<UserProfile> fromJsonParser = new JsonToModelParser<UserProfile>() {
        @Override
        public UserProfile parseJson(String json) throws ParseException {
            try {
                return fromJson(new JSONObject(json));
            } catch (JSONException e) {
                throw new ParseException(e);
            }
        }
    };

    public final static ModelToJsonParser<UserProfile> toJsonParser = new ModelToJsonParser<UserProfile>() {
        @Override
        public String parseModel(UserProfile m) {
            try {
                return new JSONStringer()
                        .object()
                        .key("id").value(m.id)
                        .key("email").value(m.email)
                        .key("username").value(m.username)
                        .key("firstName").value(m.firstName)
                        .key("lastName").value(m.lastName)
                        .key("avatarURL").value(m.avatarURL)
                        .key("location").object()
                        .key("country").value(m.country)
                        .key("city").value(m.city)
                        .endObject()
                        .endObject()
                        .toString();
            } catch (JSONException e) {
                return "";
            }
        }
    };

    public static UserProfile fromJson(JSONObject object) throws JSONException {
        String id = object.getString("id");
        String email = object.getString("email");
        String username = object.getString("username");
        String firstName = object.getString("firstName");
        String lastName = object.getString("lastName");
        String avatarURL = object.getString("avatarURL");

        JSONObject location = object.getJSONObject("location");
        String country = location.getString("country");
        String city = location.getString("city");

        return new UserProfile(country, city, id, email, username, firstName, lastName, avatarURL);
    }
}
