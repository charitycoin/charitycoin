package in.charityco.android.charitycoin.api;


import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.charityco.android.charitycoin.api.http.Http;
import in.charityco.android.charitycoin.api.model.Cause;
import in.charityco.android.charitycoin.api.model.FeedItem;
import in.charityco.android.charitycoin.api.model.User;
import in.charityco.android.charitycoin.api.model.UserProfile;
import in.charityco.android.charitycoin.api.model.parser.ParseException;
import in.charityco.android.charitycoin.util.Log;
import in.charityco.android.charitycoin.util.Task;

public class CharityCoinApi {

    private static final Log log = Log.getLog(CharityCoinApi.class.getSimpleName());

    private static class Endpoint {
        private static final String BASE = "https://charityco.in/api/v1";

        public static final String LOGIN = BASE + "/login/facebook";
        public static final String CAUSE = BASE + "/causes";
        public static final String FEED = BASE + "/feed";
        public static final String SHARE_CAUSE = CAUSE + "/%s/share";
        public static final String USER_PROFILE = BASE + "/users/%s";
        public static final String CAUSE_ARCHIVE = BASE + "/cause-archive";
    }

    private final Http http;
    private final ApiStorage storage;

    private static CharityCoinApi instance;

    private class LoginListener implements Task.OnDoneListener<Http.Result> {

        private Task.OnDoneListener<User> callback;
        private User user;

        public LoginListener(Task.OnDoneListener<User> callback) {
            this.callback = callback;
        }

        private void getUserProfile() {
            CharityCoinApi.this.getUserProfile(user.userId, new Task.OnDoneListener<UserProfile>() {
                @Override
                public void onDone(UserProfile userProfile) {
                    storage.storeUserProfile(userProfile);
                    callback.onDone(user);
                }
            });
        }

        @Override
        public void onDone(Http.Result result) {
            try {
                JSONObject object = new JSONObject(result.output);
                storage.storeAccessToken(object.getString("token"));

                user = User.fromJsonObject(object.getJSONObject("user"));
                storage.storeUser(user);

                getUserProfile();
            } catch (JSONException | ParseException e) {
                log.error(e);
            }
        }
    }

    private CharityCoinApi(Context context) {
        this.http = new Http(context);
        this.storage = new ApiStorage(context);

        log.info("Access token", storage.getAccessToken());
    }

    public static void init(Context context) {
        if (instance == null) {
            instance = new CharityCoinApi(context);
        }
    }

    public static CharityCoinApi getInstance(Context context) {
        init(context);
        return instance;
    }

    public void login(String fbAccessToken, final Task.OnDoneListener<User> listener) {
        if (storage.hasAccessToken()) {
            listener.onDone(storage.getUser());
            return;
        }

        Map<String, String> params = new HashMap<>();
        params.put("accessToken", fbAccessToken);

        JSONObject data = new JSONObject(params);

        http.post(Endpoint.LOGIN, data,
                getDefaultHeaders(),
                new LoginListener(listener));
    }

    public void getUserProfile(String userId, Task.OnDoneListener<UserProfile> listener) {
        http.get(String.format(Endpoint.USER_PROFILE, userId), getDefaultHeaders(),
                new ParseModelOnDone<>(UserProfile.fromJsonParser, listener));
    }

    public void isLoggedIn(Task.OnDoneListener<Boolean> listener) {
        String accessToken = storage.getAccessToken();
        if (accessToken == null) {
            listener.onDone(false);
        } else {
            // TODO make an API request to verify that the token is still valid
            listener.onDone(true);
        }
    }

    public void getCause(String id, Task.OnDoneListener<Cause> listener) {
        http.get(Endpoint.CAUSE + "/" + id,
                getDefaultHeaders(),
                new ParseModelOnDone<>(Cause.fromJsonParser, listener));
    }

    public void getCausesByCategory(String category, Task.OnDoneListener<List<Cause>> listener) {
        String url = Endpoint.CAUSE;
        if (category != null) {
            url += "?category=" + category;
        }

        http.get(url,
                getDefaultHeaders(),
                new ParseModelOnDone<>(Cause.fromJsonListParser, listener));
    }

    public void supportCause(final Cause cause, final Task.OnDoneListener<Boolean> listener) {
        http.put(Endpoint.CAUSE + "/" + cause.id + "/support",
                null,
                getDefaultHeaders(),
                new Task.OnDoneListener<Http.Result>() {
                    @Override
                    public void onDone(Http.Result result) {
                        boolean status = false;

                        try {
                            JSONObject object = new JSONObject(result.output);
                            status = object.getBoolean("result");
                        } catch (JSONException e) {
                            log.error(e);
                        }

                        storage.setSupportedCause(cause);
                        listener.onDone(status);
                    }
                });
    }

    public void getFeed(final Task.OnDoneListener<List<FeedItem>> listener) {
        http.get(Endpoint.FEED, getDefaultHeaders(),
                new ParseModelOnDone<>(FeedItem.fromJsonListParser, listener));
    }

    public void shareCause(Cause cause, final Task.OnDoneListener<Boolean> listener) {
        http.post(String.format(Endpoint.SHARE_CAUSE, cause.id), null, getDefaultHeaders(),
                new Task.OnDoneListener<Http.Result>() {
                    @Override
                    public void onDone(Http.Result result) {
                        boolean successful = false;
                        if (result.successful) {
                            try {
                                JSONObject json = new JSONObject(result.output);
                                successful = json.getBoolean("result");
                            } catch (JSONException e) {
                                log.error(e);
                            }
                        }

                        listener.onDone(successful);
                    }
                });
    }

    public void getArchive(final Task.OnDoneListener<List<Cause>> listener) {
        http.get(Endpoint.CAUSE_ARCHIVE + "?fields=" +
                "_id,goal,title,short_description,charity_id,title,picture_url,start_date,end_date," +
                "category,charity_name,donation", getDefaultHeaders(),
                new ParseModelOnDone<>(Cause.fromJsonListParser, listener));
    }

    public User getUser() {
        return storage.getUser();
    }

    public UserProfile getUserProfile() {
        return storage.getUserProfile();
    }

    public Cause getSupportedCause() {
        return storage.getSupportedCause();
    }

    public boolean hasSupportedCause() {
        return storage.hasSupportedCause();
    }

    public void setMinerCPU(int percent) {
        storage.setMinerCPU(percent);
    }

    public int getMinerCPU() {
        return storage.getMinerCPU();
    }

    private Map<String, String> getDefaultHeaders() {
        Map<String, String> headers = Http.arrayToHeaderMap("Content-Type", "application/json");

        String accessToken = storage.getAccessToken();
        if (accessToken != null) {
            headers.put("X-Access-Token", accessToken);
        }

        return headers;
    }
}
