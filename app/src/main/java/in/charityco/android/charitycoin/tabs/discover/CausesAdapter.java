package in.charityco.android.charitycoin.tabs.discover;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.charityco.android.charitycoin.R;
import in.charityco.android.charitycoin.api.model.Cause;
import in.charityco.android.charitycoin.util.Layout;


class CausesAdapter extends BaseAdapter implements Filterable {

    private final Context context;

    private final List<Cause> originalCauses;
    private List<Cause> causes;

    private Filter filter;

    public CausesAdapter(Context context, List<Cause> causes) {
        this.context = context;

        this.originalCauses = causes;
        this.causes = causes;
    }

    @Override
    public int getCount() {
        return causes.size();
    }

    @Override
    public Cause getItem(int position) {
        return causes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.cause_item, parent, false);
        }

        Cause cause = getItem(position);

        TextView friendsCount = (TextView) view.findViewById(R.id.cause_friends_supporters);

        if (cause.friends > 0) {
            friendsCount.setVisibility(View.VISIBLE);

            friendsCount.setText(context.getResources().getQuantityString(
                    R.plurals.cause_friends_supporters, cause.friends, cause.friends));
        } else {
            friendsCount.setVisibility(View.GONE);
        }

        Layout.fillCauseCard(cause, view);

        return view;
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CausesFilter();
        }

        return filter;
    }

    private class CausesFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            String category;
            if (constraint == null || constraint.length() == 0) {
                category = "ANY";
            } else {
                category = constraint.toString();
            }

            if (category.equalsIgnoreCase("ANY")) {
                results.values = originalCauses;
                results.count = causes.size();
            } else {
                List<Cause> filtered = new ArrayList<>();
                for (Cause cause : originalCauses) {
                    if (cause.category.equalsIgnoreCase(category)) {
                        filtered.add(cause);
                    }
                }

                results.values = filtered;
                results.count = filtered.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            causes = (List<Cause>) results.values;
            notifyDataSetChanged();
        }
    }
}
