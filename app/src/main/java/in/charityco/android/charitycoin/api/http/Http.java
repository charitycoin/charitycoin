package in.charityco.android.charitycoin.api.http;


import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

import in.charityco.android.charitycoin.util.LoadCertTask;
import in.charityco.android.charitycoin.util.Log;
import in.charityco.android.charitycoin.util.Task;

public class Http {

    private final Log log = Log.getLog(getClass().getSimpleName());

    private SSLSocketFactory sslSocketFactory;

    private final Context context;

    public Http(Context context) {
        this.context = context;
    }

    private void initSSL(final Task.OnDoneListener<Void> listener) {
        log.info("Initializing SSL");

        if (sslSocketFactory != null) {
            log.info("SSL already initialized");
            listener.onDone(null);
        }

        new LoadCertTask(context, new Task.OnDoneListener<SSLContext>() {
            @Override
            public void onDone(SSLContext sslContext) {
                sslSocketFactory = sslContext.getSocketFactory();
                log.info("Done loading SSL certificates, successful =", sslSocketFactory != null);
                listener.onDone(null);
            }
        }).execute();
    }

    private void sendRequest(final String endpoint, final HttpMethod httpMethod, final Object data, final Map<String, String> headers,
                             final Task.OnDoneListener<Result> listener) {
        initSSL(new Task.OnDoneListener<Void>() {
            @Override
            public void onDone(Void aVoid) {
                new HttpTask(endpoint, httpMethod, data, headers, sslSocketFactory, listener)
                        .execute();
            }
        });
    }

    public void get(String endpoint, Map<String, String> headers,
                    Task.OnDoneListener<Result> listener) {
        sendRequest(endpoint, HttpMethod.GET, null, headers, listener);
    }

    public void post(String endpoint, Object data, Map<String, String> headers,
                     Task.OnDoneListener<Result> listener) {
        sendRequest(endpoint, HttpMethod.POST, data, headers, listener);
    }

    public void put(String endpoint, Object data, Map<String, String> headers,
                    Task.OnDoneListener<Result> listener) {
        sendRequest(endpoint, HttpMethod.PUT, data, headers, listener);
    }

    public static Map<String, String> arrayToHeaderMap(String... args) {
        if (args.length % 2 == 1) {
            throw new RuntimeException("Must pass an even number of arguments");
        }

        Map<String, String> headers = new HashMap<>();

        for (int i = 0; i < args.length; i += 2) {
            headers.put(args[i], args[i + 1]);
        }

        return headers;
    }

    public static class Result {
        public final boolean successful;
        public final String error;
        public final String output;

        public Result(String error, String output) {
            this.successful = error == null;
            this.error = error;
            this.output = output;
        }
    }
}
