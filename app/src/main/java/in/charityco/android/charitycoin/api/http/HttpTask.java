package in.charityco.android.charitycoin.api.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;

import in.charityco.android.charitycoin.util.Task;


public class HttpTask extends Task<Void, Void, Http.Result> {

    final String endpoint;
    final HttpMethod method;
    final Object data;
    final Map<String, String> headers;

    final SSLSocketFactory sslSocketFactory;

    public HttpTask(String endpoint, HttpMethod method, Object data, Map<String, String> headers,
                    SSLSocketFactory factory,
                    OnDoneListener<Http.Result> listener) {
        super(listener);

        this.sslSocketFactory = factory;

        this.endpoint = endpoint;
        this.method = method;
        this.data = data;
        this.headers = headers;
    }

    private HttpURLConnection prepareConnection(String endpoint, HttpMethod method, Map<String, String> headers)
            throws IOException {

        URL url = new URL(endpoint);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

        connection.setSSLSocketFactory(this.sslSocketFactory);

        connection.setRequestMethod(method.toString());
        connection.setDoOutput(method != HttpMethod.GET);

        if (headers != null) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                connection.setRequestProperty(entry.getKey(), entry.getValue());
            }
        }

        return connection;
    }

    private void writeData(HttpURLConnection connection, Object data) throws IOException {
        if (data == null) {
            return;
        }

        byte[] bytes = String.valueOf(data).getBytes();

        connection.setFixedLengthStreamingMode(bytes.length);
        connection.getOutputStream().write(bytes);
    }

    private String readStream(InputStream stream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        StringBuilder content = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            content.append(line).append('\n');
        }
        reader.close();

        return content.toString();
    }

    private String readData(HttpURLConnection connection) throws IOException {
        return readStream(connection.getInputStream());
    }

    private String readError(HttpURLConnection connection) throws IOException {
        return readStream(connection.getErrorStream());
    }

    @Override
    protected Http.Result doInBackground(Void... params) {
        HttpURLConnection connection = null;
        Http.Result result = null;

        log.info(method, endpoint);

        try {
            connection = prepareConnection(endpoint, method, headers);
            if (connection.getDoOutput() && data != null) {
                log.info(method, endpoint, "data =", data);
                writeData(connection, data);
            }


            if (connection.getResponseCode() == 200) {
                result = new Http.Result(null, readData(connection));
            } else {
                result = new Http.Result(readError(connection), null);
            }

            log.info(method, endpoint, "success =", result.successful, "output =",
                    result.successful ? result.output : result.error);
        } catch (IOException e) {
            log.error(e);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        return result;
    }
}
