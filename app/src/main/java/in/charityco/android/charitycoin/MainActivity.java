package in.charityco.android.charitycoin;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import in.charityco.android.charitycoin.miner.MinerService;
import in.charityco.android.charitycoin.tabs.TabsFragment;
import in.charityco.android.coinminer.CPUMinerRunnable;

public class MainActivity extends FragmentActivity implements ShowCauseDetails {

    private CauseDetailsFragment causeDetailsFragment;
    private TabsFragment tabsFragment;

    FragmentManager.OnBackStackChangedListener backStackChangedListener = new FragmentManager.OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            getActionBar().setDisplayHomeAsUpEnabled(getSupportFragmentManager().getBackStackEntryCount() > 0);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tabsFragment = new TabsFragment();
        causeDetailsFragment = new CauseDetailsFragment();

        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, tabsFragment)
                .add(android.R.id.content, causeDetailsFragment)
                .hide(causeDetailsFragment)
                .commit();

        getSupportFragmentManager().addOnBackStackChangedListener(backStackChangedListener);
    }

    @Override
    public boolean onNavigateUp() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();

            return true;
        }

        return super.onNavigateUp();
    }

    @Override
    public void showCauseDetails(String causeId) {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.fade_out)
                .addToBackStack(null)
                .hide(tabsFragment)
                .show(causeDetailsFragment)
                .commit();

        causeDetailsFragment.setCauseId(causeId);
    }
}
