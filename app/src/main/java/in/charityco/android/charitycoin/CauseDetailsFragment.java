package in.charityco.android.charitycoin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import in.charityco.android.charitycoin.api.CharityCoinApi;
import in.charityco.android.charitycoin.api.model.Cause;
import in.charityco.android.charitycoin.util.LoadBitmap;
import in.charityco.android.charitycoin.util.Log;
import in.charityco.android.charitycoin.util.Task;

public class CauseDetailsFragment extends Fragment {

    private static final Log log = Log.getLog(CauseDetailsFragment.class.getSimpleName());

    private ProgressDialog dialog;

    private String causeId;
    private Cause cause;

    private Cause supportedCause;

    private boolean isSupported = false;

    private ImageButton supportBtn;

    private View.OnClickListener onSupportClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CharityCoinApi.getInstance(getActivity()).supportCause(cause, new Task.OnDoneListener<Boolean>() {
                @Override
                public void onDone(Boolean status) {
                    if (isSupported) {
                        log.info("Attempt to support an already supported cause", cause.id, "Cancelling");
                        return;
                    }

                    if (!status) {
                        Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                    } else {
                        supportedCause = cause;
                        supportBtn.setImageResource(R.drawable.voted);
                    }
                }
            });
        }
    };

    public CauseDetailsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        supportedCause = CharityCoinApi.getInstance(getActivity()).getSupportedCause();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_cause_details, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_share_cause) {
            shareCause();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void shareCause() {
        CharityCoinApi.getInstance(getActivity()).shareCause(cause, new Task.OnDoneListener<Boolean>() {
            @Override
            public void onDone(Boolean successful) {
                if (successful) {
                    Toast.makeText(getActivity(), "Shared", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void setCauseId(String causeId) {
        this.causeId = causeId;
        isSupported = false;

        if (isResumed()) {
            loadCause(causeId);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (causeId == null) {
            return;
        }

        if (cause != null && !causeId.equals(cause.id)) {
            loadCause(causeId);
        }
    }

    private void loadCause(String causeId) {
        onStartLoading();

        CharityCoinApi.getInstance(getActivity()).getCause(causeId, new Task.OnDoneListener<Cause>() {
            @Override
            public void onDone(Cause object) {
                cause = object;
                isSupported = supportedCause != null && supportedCause.id.equals(cause.id);
                updateUI(object);
            }
        });
    }

    private void onStartLoading() {
        View view = getView();
        if (view != null) {
            view.setVisibility(View.INVISIBLE);
        }

        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        } else {
            Activity activity = getActivity();
            if (activity != null) {
                dialog = ProgressDialog.show(activity, "", "", true);
            }
        }
    }

    private void onStopLoading() {
        View view = getView();
        if (view != null) {
            getView().setVisibility(View.VISIBLE);
        }

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cause_details, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        supportBtn = (ImageButton) view.findViewById(R.id.cause_support_button);
        supportBtn.setOnClickListener(onSupportClicked);

        view.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void updateUI(final Cause cause) {
        final View view = getView();
        if (cause == null || view == null) {
            return;
        }

        if (isSupported) {
            supportBtn.setImageResource(R.drawable.voted);
        } else {
            supportBtn.setImageResource(R.drawable.vote);
        }

        Resources resources = getActivity().getResources();
        ((TextView) view.findViewById(R.id.cause_title)).setText(cause.title);

        ((TextView) view.findViewById(R.id.cause_charity)).setText(
                resources.getString(R.string.cause_started_by_charity, cause.charityName));

        ((TextView) view.findViewById(R.id.cause_supporters_count)).setText(
                resources.getQuantityString(R.plurals.cause_join_supporter, cause.supporters, cause.supporters));

        int days = cause.getDaysLeft();
        ((TextView) view.findViewById(R.id.cause_time)).setText(
                resources.getQuantityString(R.plurals.cause_days_left, days, days));

        ((TextView) view.findViewById(R.id.cause_description)).setText(cause.shortDescription);
        ((TextView) view.findViewById(R.id.cause_funding)).setText(
                cause.goalUnit + (cause.goal - cause.fundsRaised));

        // add +1 to progress so that even empty causes show a little green bar :)
        ((ProgressBar) view.findViewById(R.id.cause_funding_progress)).setProgress(
                1 + cause.getGoalPercentDone());

        final ImageView imageView = (ImageView) view.findViewById(R.id.cause_picture);

        if (cause.pictureUrl != null) {
            LoadBitmap.loadIntoImageView(imageView, cause.pictureUrl);
        }

        onStopLoading();
    }
}
