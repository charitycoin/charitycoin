package in.charityco.android.charitycoin.tabs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.charityco.android.charitycoin.R;
import in.charityco.android.charitycoin.api.CharityCoinApi;


public class TabsFragment extends Fragment {

    public TabsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tabs, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ViewPager tabPager = (ViewPager) view.findViewById(R.id.main_tabs_pager);
        tabPager.setAdapter(new TabsAdapter(getActivity(), getChildFragmentManager()));

        if (!CharityCoinApi.getInstance(getActivity()).hasSupportedCause()) {
            tabPager.setCurrentItem(1, true);
        }

        final SlidingTabLayout tabs = (SlidingTabLayout) view.findViewById(R.id.main_tabs_header);

        tabs.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        tabs.setSelectedIndicatorColors(getResources().getColor(R.color.accent));
        tabs.setBottomBorderColor(getResources().getColor(R.color.accent));
        tabs.setDividerColors(getResources().getColor(android.R.color.transparent));
        tabs.setFillViewport(true);
        tabs.setCustomTabView(R.layout.tab_title, R.id.tab_title);

        tabs.setViewPager(tabPager);
    }

}
