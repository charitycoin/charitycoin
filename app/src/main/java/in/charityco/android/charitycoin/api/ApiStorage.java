package in.charityco.android.charitycoin.api;


import android.content.Context;
import android.content.SharedPreferences;

import in.charityco.android.charitycoin.api.model.Cause;
import in.charityco.android.charitycoin.api.model.User;
import in.charityco.android.charitycoin.api.model.UserProfile;
import in.charityco.android.charitycoin.api.model.parser.JsonToModelParser;
import in.charityco.android.charitycoin.api.model.parser.ParseException;
import in.charityco.android.charitycoin.util.Log;

class ApiStorage {

    private static final Log log = Log.getLog(ApiStorage.class.getSimpleName());

    private static final String PREF_FILE = ApiStorage.class.getPackage().getName() + ".preference";

    private static final String PREF_ACCESS_TOKEN = PREF_FILE + ".access_token";
    private static final String PREF_USER = PREF_FILE + ".user";
    private static final String PREF_USER_PROFILE = PREF_FILE + ".user.profile";
    private static final String PREF_CAUSE = PREF_FILE + ".cause";
    private static final String PREF_MINER_CPU = PREF_FILE + ".miner.cpu";

    private final Context context;

    private User user;
    private UserProfile profile;
    private Cause cause;
    private String accessToken;

    public ApiStorage(Context context) {
        this.context = context;
    }

    public void storeAccessToken(String accessToken) {
        this.accessToken = accessToken;

        getSharedPreferences().edit()
                .putString(PREF_ACCESS_TOKEN, accessToken)
                .commit();
    }

    public String getAccessToken() {
        if (accessToken == null) {
            accessToken = getSharedPreferences().getString(PREF_ACCESS_TOKEN, null);
        }

        return accessToken;
    }

    public boolean hasAccessToken() {
        return accessToken != null || getSharedPreferences().contains(PREF_ACCESS_TOKEN);
    }

    public void storeUser(User user) {
        this.user = user;

        getSharedPreferences().edit()
                .putString(PREF_USER, user.toString())
                .commit();
    }

    public void storeUserProfile(UserProfile profile) {
        this.profile = profile;

        getSharedPreferences().edit()
                .putString(PREF_USER_PROFILE, UserProfile.toJsonParser.parseModel(profile))
                .commit();
    }

    public User getUser() {
        if (user == null) {
            user = restoreUser();
        }

        return user;
    }

    public UserProfile getUserProfile() {
        if (profile == null) {
            profile = restoreProfile();
        }

        return profile;
    }

    public boolean hasSupportedCause() {
        return getSharedPreferences().getString(PREF_CAUSE, null) != null;
    }

    public Cause getSupportedCause() {
        if (cause == null) {
            cause = restoreCause();
        }

        return cause;
    }

    public void setSupportedCause(Cause cause) {
        getSharedPreferences().edit()
                .putString(PREF_CAUSE, Cause.toJsonParser.parseModel(cause))
                .commit();
    }

    public void setMinerCPU(int cpu) {
        getSharedPreferences().edit()
                .putInt(PREF_MINER_CPU, cpu)
                .commit();
    }

    public int getMinerCPU() {
        return getSharedPreferences().getInt(PREF_MINER_CPU, 10);
    }

    private UserProfile restoreProfile() {
        return restore(PREF_USER_PROFILE, UserProfile.fromJsonParser);
    }

    private User restoreUser() {
        return restore(PREF_USER, User.fromJsonParser);
    }

    private Cause restoreCause() {
        return restore(PREF_CAUSE, Cause.fromJsonParser);
    }

    private <T> T restore(String pref, JsonToModelParser<T> parser) {
        String json = getSharedPreferences().getString(pref, null);
        T model = null;

        if (json != null) {
            try {
                model = parser.parseJson(json);
            } catch (ParseException e) {
                log.error(e);
            }
        }

        return model;
    }

    private SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
    }
}
